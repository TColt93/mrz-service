import os
import cv2
import numpy as np
import argparse
from random import randint
import os
import app.homography as homography
import app.detect_mrz as MRZer
import app.tesseract_extract as tess
import json
import datetime
import imutils
import base64
import re

class MRZ():
    def __init__(self, passImage, type):
        self.debug = "False"

        passimage = base64.b64decode(passImage)
        passnparr = np.frombuffer(passimage, np.uint8)
        self.passImage = cv2.imdecode(passnparr, cv2.IMREAD_COLOR)
        self.type = type
        self.mrz_prediction = None
        self.mrz_image = None

    # This function is used to do validations and OCR of the given files
    # Returns the consolidated predictions and consolidated image arrays
    def Full_MRZ(self):
        # Read in the orginial image from file path in color
        img = self.passImage
        retval, buffer = cv2.imencode('.png', img)
        img_as_text = base64.b64encode(buffer)
        self.original_image = str(img_as_text)

        # Input the image we have just read into the Homography function
        # Return best image and type of ID Book
        homo = homography.Homography(self.passImage, self.type)
        homography_object = homo.Perfect_Image()

        # Input the best image and ID Type to the OCR function to extract all text and numbers
        # Return the OCRImg array with bounding boxes and character predictions and the characters extracted in json format
        mrz = MRZer.MRZ_Extract(homography_object.fixed_img)

        if hasattr(mrz, 'size'):
            response = tess.find_text(mrz)
            valid = True
        else:
            response = 'Invalid document'
            valid = False

        response = MRZ_parser(response)

        return response, valid

class MRZ_parser():
    def __init__(self, text):
        text = text.replace(' ', '')
        text_arr = text.splitlines()
        if len(text_arr) == 2:
            count = 0
            for line in text_arr:
                # first line of mrz
                if count == 0:
                    # type
                    if line[:1] == 'P':
                        self.type = 'Passport'
                        line = line[2:]

                    # country
                    self.country = line[0:3]
                    line = line[3:]

                    # surname
                    match = re.search(r'<<', line, re.M)
                    if match is not None:
                        matchregs1 = match.regs[0][1]
                        matchregs4 = matchregs1-2
                        self.surname = line[:matchregs4].replace('<',' ')
                        line = line[matchregs1:]
                    
                    # surname
                    match = re.search(r'<<', line, re.M)
                    if match is not None:
                        matchregs1 = match.regs[0][0]
                        matchregs = matchregs1+1
                        self.name = line[:matchregs-1].replace('<',' ')
                
                # second line of mrz
                if count == 1:
                    self.doc_num = line[:9].replace('<','')
                    self.nationality = line[10:13]
                    self.dob = line[13:15] + '-' + line[15:17] + '-' + line[17:19]
                    self.gender = line[20:21]
                    self.expiry_date = line[21:23] + '-' + line[23:25] + '-' + line[25:27]
                    self.optional_data = line[28:len(line)-2].replace('<','')
                    self.overall_check = line[len(line) - 1]


                count += 1