from flask_api import FlaskAPI
from flask import request, jsonify, abort
#local import
from instance.config import app_config
import json
from datetime import datetime
from app.response_func import responser

def create_app(config_name):
    from app.MRZ_Get import MRZ
    from app.response_func import responser

    app = FlaskAPI(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    @app.route('/MRZextraction/', methods=['POST'])
    def MRZ_extract():
        idImage = request.data.get("idImage","")
        type = request.data.get("type","")

        if idImage and type:
            mrz = MRZ(idImage, type)
            obj = mrz.Full_MRZ()

            response = responser(obj)

            if obj[1] == False:
                response.status_code = 400
                response.timestamp = str(datetime.now())
            else:
                response.status_code = 200
                response.timestamp = str(datetime.now())

            return response
    return app