import cv2
import numpy as np
import argparse
from random import randint
import os


class Homography():
    def __init__(self, MRZImage, type):
        self.Original_Image = MRZImage
        self.MAX_FEATURES = 2000
        self.GOOD_MATCH_PERCENT = 0.075
        self.type = type

    def align_images(self, template):

        #Make the Template Image the standard size to ensure the best results when finding which Template to use
        shape = template.shape
        height = 650
        r = height / float(shape[0])
        dim = (int(shape[1] * r), height)

        im2 = cv2.resize(template, dim, interpolation = cv2.INTER_AREA)


        # Convert images to grayscale
        im1Gray = cv2.cvtColor(self.Original_Image, cv2.COLOR_BGR2GRAY)
        im2Gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)

        # Detect ORB features and compute descriptors.
        orb = cv2.ORB_create(self.MAX_FEATURES)
        keypoints1, descriptors1 = orb.detectAndCompute(im1Gray, None)
        keypoints2, descriptors2 = orb.detectAndCompute(im2Gray, None)

        # Match features
        matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
        matches = matcher.match(descriptors1, descriptors2, None)

        # Sort matches by score
        matches.sort(key=lambda x: x.distance, reverse=False)

        # Remove not so good matches
        numGoodMatches = int(len(matches) * self.GOOD_MATCH_PERCENT)
        matches = matches[:numGoodMatches]

        # Draw top matches (Use for testing purposes)
        # imMatches = cv2.drawMatches(im1, keypoints1, im2, keypoints2, matches, None)
        # cv2.imwrite("matches.jpg", imMatches)

        # Extract location of good matches
        points1 = np.zeros((len(matches), 2), dtype=np.float32)
        points2 = np.zeros((len(matches), 2), dtype=np.float32)

        for i, match in enumerate(matches):
            points1[i, :] = keypoints1[match.queryIdx].pt
            points2[i, :] = keypoints2[match.trainIdx].pt

        # Find homography
        h, mask = cv2.findHomography(points1, points2, cv2.RANSAC)

        # Use homography
        height, width = im2.shape[:2]
        im1Reg = cv2.warpPerspective(self.Original_Image, h, (width, height))

        return im1Reg

    def Perfect_Image(self):

        template = self.get_template()

        Fixed_Img = self.align_images(template)

        self.fixed_img = Fixed_Img

        cv2.imshow('homo', Fixed_Img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        return self

    def get_template(self):
        template_type = self.type

        if template_type == 'AUSTRIA': template_name = 'Austria_Template.jpg'
        elif template_type == 'BELGIUM': template_name = 'Belgium_Template.jpg'
        elif template_type == 'CROATIA_CARD': template_name = 'Croatia_Card_Template.jpg'
        elif template_type == 'CROATIA_BOOK': template_name = 'Croatia_Book_Template.jpg'
        elif template_type == 'FRANCE': template_name = 'France_Template.jpg'
        elif template_type == 'GREAT_BRITAIN': template_name = 'Great_Britain_Template.jpg'
        elif template_type == 'KUWAIT': template_name = 'Kuwait_Template.jpg'
        elif template_type == 'SA': template_name = 'SA_Template2.png'

        return cv2.imread('Templates/{}'.format(template_name), cv2.IMREAD_COLOR)
