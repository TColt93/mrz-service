from flask import jsonify

def responser(returned):
    if returned[1] == True:
        ret = returned[0]
        response = jsonify({
            'MRZ_Extraction': {
            'nationality': ret.nationality,
            'date_of_birth': ret.dob,
            'names': ret.name,
            'surname': ret.surname,
            'gender': ret.gender,
            'number': ret.doc_num
            }
        })
    if returned[1] == False:
        response = jsonify({
            'MRZ_Extraction': 'Could not find mrz'
        })
        
    return response