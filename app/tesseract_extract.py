import pytesseract
import numpy as np
import cv2
from PIL import Image
import imutils
from imutils import contours
import passporteye

def get_string(idImage):

    # Define config parameters.
    # '-l eng'  for using the English language
    # '--oem 1' for using LSTM OCR Engine
    config = ('-l eng --oem 1 --psm 3')

    # shape = idImage.shape[:2]
    # Top = int(shape[0] * 0.2)
    # Bottom = int(shape[0] * 0.95)

    # idImage = idImage[Top:Bottom,0:shape[1]]

    # cv2.imshow('before read', idImage)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # Recognize text with tesseract for python
    result = pytesseract.image_to_string(idImage, config=config)

    return result

def find_text(mrz):
    debug = "False"

    # boundary = [([0,0,0],[160,160,160])]

    # for (lower, upper) in boundary:
    #     # create NumPy arrays from the boundaries
    #     lower = np.array(lower, dtype = 'uint8')
    #     upper = np.array(upper, dtype = 'uint8')
    #     gray_mrz = cv2.cvtColor(mrz, cv2.COLOR_BGR2GRAY)

    #     (h,w) = gray_mrz.shape[:2]
    #     blank_image = np.zeros((h,w,3), np.uint8)

    #     # find the colors within the specified boundaries and apply the mask
    #     mask = cv2.inRange(mrz, lower, upper)
    #     img = cv2.bitwise_not(mrz, blank_image, mask = mask)

    #     # Threshold RoI for sorting
    #     ret, white_on_black = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV)

    #     if debug == "True":
    #         cv2.imshow('mrz', white_on_black)
    #         cv2.waitKey(0)
    #         cv2.destroyAllWindows()

    # Get the image from the previous array
    # roiImage = Image.fromarray(white_on_black)

    # # Passport eye needs a PDF format - Save and delete after
    # roiImage.save('mrz.pdf', "PDF", resolution=100.0)

    # # Get the parsed MRZ text
    # parsed = passporteye.read_mrz('mrz.pdf')

    # os.remove('mrz.pdf')
    cv2.imwrite('pierre.png',mrz)
    cv2.imshow('mrz', mrz)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    parsed = get_string(mrz)

    return parsed