import unittest
import os
import json
import base64
from app import create_app

class MRZget_TestCase(unittest.TestCase):
    """ This class represent the Green ID test case """

    def setUp(self):
        """Define test variables and initialize app """
        self.app = create_app(config_name="testing")
        self.client = self.app.test_client

        idImage = open("examples/pierre.png","rb")
        id_image_read = idImage.read()
        self.idImage_64_encode = base64.b64encode(id_image_read)
        idImage.close()

        type = "SA"
        self.type = type

    def test_MRZ_Get(self):
        res = self.client().post('/MRZextraction/', data={"idImage":self.idImage_64_encode, "type":self.type, "debug":self.debug})

        if res.status_code == 400:     
            self.assertEqual(res.status_code, 400)
            self.assertIn("MRZ_Extraction", str(res.data))
        elif res.status_code == 200:
            self.assertEqual(res.status_code, 200)
            self.assertIn("MRZ_Extraction", str(res.data))
        # print(str(res.data))

    # def tearDown(self):
    #     """ tear down all initialized variables """
        
# Make the tests conveniently executable
if __name__ == '__main__':
    unittest.main()
